__author__ = 'matvey'

import socket
import threading

HOST = ''  # or socket.gethostname()
PORT = 8080
ADDR = (HOST, PORT)
LIMIT = 10

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

users = {}


def hasuser(_id):
    return _id in users


def sendto(conn, msg):
    conn.send(msg.encode("utf-8"))


def getstr(conn):
    udata = ""
    data = conn.recv(1024)
    if data:
        udata = data.decode("utf-8")
    return udata


def workwith(conn, addr):
    _id = getstr(conn)
    users[_id] = conn
    print('userID: ' + _id)
    # print 'keys %s' % users.keys()
    while 1:
        msg = getstr(conn)
        if msg == '':
            del users[_id]
            conn.close()
            print('%s - Connection closed' % _id)
            break
        if msg == 'send':
            to = getstr(conn)
            msgU = getstr(conn)
            if hasuser(to):
                sendto(users[to], _id + '<>' + msgU)
                #sendto(users[to], _id)
                #sendto(users[to], msgU)
        if msg == 'status':
            to = getstr(conn)
            if hasuser(to):
                sendto(conn, '+')
            else:
                sendto(conn, '-')
        print('Data(%s): ' % _id + msg)


def waitconnect():
    while 1:
        conn, addr = sock.accept()
        print('Connected ' + addr[0])
        t = threading.Thread(target=workwith, args=(conn, addr))
        t.daemon = True
        t.start()


def main():
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(ADDR)
    sock.listen(LIMIT)
    print('Wait for connect...')

    t = threading.Thread(target=waitconnect, args=())
    t.daemon = True
    t.start()

    while 1:
        msg = input()
        if msg == 'quit':
            sock.close()
            break
        if msg == 'list':
            print('Users - ' + str(len(users)))
            print(str(users.keys()))


main()
